package com.kkorchyts.Ex3_SentenceProcessor;

import java.util.Collections;

public class SortedSentence extends Sentence {
    public SortedSentence() {
        super();
    }

    public SortedSentence(String sentence) {
        super(sentence);
    }

    @Override
    protected void processSentence() {
        super.processSentence();
        try {
            Collections.sort(words);
        } catch (Exception e) {
            System.out.println("Возникала ошибка сортировки!");
        }
    }
}
