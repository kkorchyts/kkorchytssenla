package com.kkorchyts.Ex3_SentenceProcessor;

import java.util.Arrays;
import java.util.List;

public class Sentence {
    protected List<String> words;
    private String sentence;

    public Sentence() {
        setSentence("");
    }

    public Sentence(String sentence) {
        setSentence(sentence);
    }

    protected void processSentence() {
        words = Arrays.asList(sentence.split(" +"));
    }

    public List<String> getWords() {
        return words;
    }

    public Integer getWordsCount() {
        return words.size();
    }

    public void toUpperWordsFirstLetterInWords() {
        for (int i = 0; i < words.size(); i++) {
            if (words.get(i) != null) {
                StringBuffer bf = new StringBuffer(words.get(i));
                if (bf.length() > 0) {
                    char ch = Character.toUpperCase(bf.charAt(0));
                    bf.setCharAt(0, ch);
                    words.set(i, bf.toString());
                }
            }
        }
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
        processSentence();
    }
}
