package com.kkorchyts.Ex3_SentenceProcessor;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class SentenceProcessorApp {

    public static void main(String[] args) throws Exception {
        SentenceProcessorApp.run();
    }

    public static void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите предложение:");
        String originalSentence = scanner.nextLine();

        Sentence sentence = new Sentence(originalSentence);
        SortedSentence sortedSentence = new SortedSentence(originalSentence);
        System.out.println("Количество слов в предложении: " + sentence.getWordsCount());

        printList(sortedSentence.getWords(), "Слова в алфавитном порядке");
        sortedSentence.toUpperWordsFirstLetterInWords();
        printList(sortedSentence.getWords(), "Слова с заглавной первой буквой");
    }

    public static void printList(List<String> list, String message) {
        System.out.println("-------- Начало: " + message + "-----------");

        for (String word: list) {
            System.out.println(word);
        }

        System.out.println("-------- Окончание: " + message + "-----------");
    }

}
