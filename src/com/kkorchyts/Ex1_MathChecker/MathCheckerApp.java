package com.kkorchyts.Ex1_MathChecker;

import java.io.IOException;
import java.util.Scanner;

public class MathCheckerApp {

    public static void main(String[] args) {
        MathCheckerApp.run();
    }


    public static void run() {
        Integer integer;
        try {
            integer = readInteger();
        } catch (IOException e) {
            System.out.println("Ошибка:" + e.getMessage());
            return;
        }

        Checker evenChecker = new EvenChecker(integer);
        Checker primeChecker = new PrimeChecker(integer);

        evenChecker.printCheckingResult();
        primeChecker.printCheckingResult();
    }

    public static Integer readInteger() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целое число:");
        Integer result;
        if (scanner.hasNextInt()) {
            result = Integer.parseInt(scanner.next());
        } else {
            throw new IOException(getMessageErrorNotInteger(scanner.next()));
        }
        return result;
    }

    public static String getMessageErrorNotInteger(String string) {
        if (string.matches("-?\\d+?")) {
            return "Число введено верно!";
        } else if (string.matches("[-+]?\\d+([\\.\\,]\\d+)?")) {
            return "Число не является целым!";
        } else {
            return "Агрумент является строкой!";
        }
    }
}
