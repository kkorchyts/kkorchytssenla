package com.kkorchyts.Ex1_MathChecker;

public interface Checker {
    boolean check();

    void printCheckingResult();
}
