package com.kkorchyts.Ex1_MathChecker;

public class EvenChecker extends BaseChecker {

    public EvenChecker() {
        super();
    }

    public EvenChecker(Integer integer) {
        super(integer);
    }

    // возвращает true если число четное, иначе - false
    @Override
    public boolean check() {
        return (getInteger() % 2 == 0);
    }

    @Override
    public String getTrueMessage() {
        return "Данное число является четным.";
    }

    @Override
    public String getFalseMessage() {
        return "Данное число является нечетным.";
    }
}
