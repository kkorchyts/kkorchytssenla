package com.kkorchyts.Ex1_MathChecker;

import java.math.BigInteger;

public class PrimeChecker extends BaseChecker {
    private Integer certainty = 100;

    public PrimeChecker() {
        super();
    }

    public PrimeChecker(Integer integer) {
        super(integer);
    }

    // возвращает true если число простое, иначе - false
    @Override
    public boolean check() {
        BigInteger bigInteger = BigInteger.valueOf(getInteger());
        return bigInteger.isProbablePrime(certainty);
    }

    @Override
    public String getTrueMessage() {
        return "Данное число является простым.";
    }

    @Override
    public String getFalseMessage() {
        return "Данное число является составным.";
    }

    public Integer getCertainty() {
        return certainty;
    }

    public void setCertainty(Integer certainty) {
        this.certainty = certainty;
    }
}
