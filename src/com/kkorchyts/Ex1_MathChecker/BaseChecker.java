package com.kkorchyts.Ex1_MathChecker;

public abstract class BaseChecker implements Checker {
    private Integer integer;

    public BaseChecker() {
        setInteger(0);
    }

    public BaseChecker(Integer integer) {
        setInteger(integer);
    }

    public abstract boolean check();

    public void printCheckingResult() {
        if (check())
            System.out.println(getTrueMessage());
        else
            System.out.println(getFalseMessage());
    }

    public abstract String getTrueMessage();

    public abstract String getFalseMessage();

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }
}
