package com.kkorchyts.Ex2_Calculator;

import java.math.BigInteger;

public class ExtraInteger {
    private Integer integer;

    public ExtraInteger() {
        setInteger(0);
    }

    public ExtraInteger(Integer integer) {
        setInteger(integer);
    }

    public Integer gcd(Integer argument) {
        BigInteger a = BigInteger.valueOf(integer);
        BigInteger b = BigInteger.valueOf(argument);
        BigInteger gcd = a.gcd(b);
        Integer result = gcd.intValue();
        return result;
    }

    public Integer lcm(Integer argument) {
        Integer result = 0;
        if (integer == 0 && argument == 0) {
            return result;
        }

        Integer a = Math.abs(integer);
        Integer b = Math.abs(argument);

        Integer gcd = gcd(b);
        result = BigInteger.valueOf(a * b).divide(BigInteger.valueOf(gcd))
                .intValue();
        return result;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }
}
