package com.kkorchyts.Ex2_Calculator;

import java.io.IOException;
import java.util.Scanner;

public class CalculatorApp {

    public static void main(String[] args) {
        CalculatorApp.run();
    }

    public static void run() {
        Integer firstNumber = 0;
        Integer secondNumber = 0;

        try {
            firstNumber = readInteger("первый аргумент");
            secondNumber = readInteger("второй аргумент");
        } catch (IOException e) {
            System.out.println("Ошибка:" + e.getMessage());
            return;
        }

        ExtraInteger extraInteger = new ExtraInteger(firstNumber);

        System.out.println("НОД = " + extraInteger.gcd(secondNumber));
        System.out.println("НОК = " + extraInteger.lcm(secondNumber));
    }

    public static Integer readInteger(String message) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите " + message + " (целое число):");
        Integer result;
        if (scanner.hasNextInt()) {
            result = Integer.parseInt(scanner.next());
        } else {
            throw new IOException("Вы ввели не целое число. Программа будет закрыта.");
        }
        return result;
    }

}
