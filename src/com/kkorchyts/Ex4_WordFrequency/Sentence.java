package com.kkorchyts.Ex4_WordFrequency;

import java.util.HashMap;
import java.util.Map;

public class Sentence {
    private String sentence;

    // слова хранятся в нижнем решистре
    private Map<String, Integer> words;

    public Sentence() {
        words = new HashMap<String, Integer>();
    }

    private void generateWordsMap() {
        String[] wordsArray = sentence.toLowerCase().split(" +");
        words.clear();
        for (String str : wordsArray) {
            Integer count = words.get(str);
            if (count != null) {
                words.put(str, count + 1);
            } else
                words.put(str, 1);
        }
    }

    public Integer calculateWordFrequency(String word) {
        Integer result = words.get(word.toLowerCase());
        if (result == null)
            result = 0;
        return result;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
        generateWordsMap();
    }

}
