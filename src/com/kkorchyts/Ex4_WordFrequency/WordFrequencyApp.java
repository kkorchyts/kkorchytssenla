package com.kkorchyts.Ex4_WordFrequency;

import java.util.Scanner;

public class WordFrequencyApp {

    public static void main(String[] args) {
        WordFrequencyApp.run();
    }

    public static void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите предложение:");
        String strSentence = scanner.nextLine();
        System.out.println("Введите искомое слово:");
        String word = scanner.next();

        Sentence sentence = new Sentence();
        sentence.setSentence(strSentence);
        Integer count = sentence.calculateWordFrequency(word);
        if (count == 0) {
            System.out.println("Слово \"" + word + "\" в предложении не встречается \"");
        } else {
            System.out.println("Частота вхождения слова \"" + word + "\" в предложении равна " + count);
        }
    }
}
