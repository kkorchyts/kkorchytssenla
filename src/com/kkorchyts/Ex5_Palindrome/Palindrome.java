package com.kkorchyts.Ex5_Palindrome;

import java.util.ArrayList;
import java.util.List;

public class Palindrome {
    public static boolean isPalindrome(Integer argument) {
        String strOriginalArgument = String.valueOf(argument);
        StringBuilder strArgument = new StringBuilder(strOriginalArgument);
        StringBuilder reverseArgument = strArgument.reverse();
        Boolean result = strOriginalArgument.equals(reverseArgument.toString());
        return result;
    }

    public static List<Integer> findPalindromesInRange(Integer startOfRange, Integer endOfRange) {
        List<Integer> palindromes = new ArrayList<Integer>();
        for (int i = startOfRange; i <= endOfRange; i++) {
            if (Palindrome.isPalindrome(i))
                palindromes.add(i);
        }
        return palindromes;
    }

}
