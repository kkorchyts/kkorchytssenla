package com.kkorchyts.Ex5_Palindrome;

import java.util.List;
import java.util.Scanner;

public class PalindromeApp {
    private static final int MIN_RANGE = 0;
    private static final int MAX_RANGE = 100;

    public static void main(String[] args) {
        PalindromeApp.run();
    }

    public static void run() {
        int range = getRange();
        if (range == -1) {
            System.out.println("Вы ввели неверное значение верхней границы диапозона!");
            return;
        }
        List<Integer> palindromes = Palindrome.findPalindromesInRange(MIN_RANGE, range);
        printPalindromes(palindromes);
    }

    // функция возвращает верхнюю границу диапозона, не превышающею 100.
    // если введено неверное значение, функция возвращает -1
    public static int getRange() {
        System.out.println("Введите, пожалуйста, верхнюю границу диапозона (положительное число, не превышающее 100):");
        Scanner scanner = new Scanner(System.in);
        int result = -1;
        if (scanner.hasNextInt()) {
            Integer range = scanner.nextInt();
            if (range >= 0 && range <= MAX_RANGE) {
                result = range;
            }
        }
        return result;
    }

    public static void printPalindromes(List<Integer> palindromes) {
        System.out.println("------ Список палиндромов в вашем диапозоне -------");
        for (Integer palindrome : palindromes) {
            System.out.println(palindrome);
        }
    }
}
