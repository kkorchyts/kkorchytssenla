package com.kkorchyts.Ex6_Knapsack;

import java.util.ArrayList;
import java.util.List;

public class Packer {
    private void checkBackPack(List<Item> items, Knapsack currentPack) {
        if (Knapsack.isSuitableItemList(items, currentPack.getMaxWeight()) &&
                Knapsack.getItemListCost(items) > currentPack.getCurrentCost()) {
                currentPack.replaceItems(items);
        }
    }

    private void processPacking(List<Item> items, Knapsack currentKnapsack) {
        if (items == null)
            return;
        if (!items.isEmpty())
            checkBackPack(items, currentKnapsack);

        for (int i = 0; i < items.size(); i++) {
            List<Item> newItems = new ArrayList<Item>(items);
            newItems.remove(i);
            processPacking(newItems, currentKnapsack);
        }
    }

    public Knapsack packItems(List<Item> items, Knapsack knapsack) {
        Integer maxBackPackWeight = knapsack.getMaxWeight();
        if (items == null || maxBackPackWeight == 0)
            return knapsack;
        processPacking(items, knapsack);
        return knapsack;
    }

}

