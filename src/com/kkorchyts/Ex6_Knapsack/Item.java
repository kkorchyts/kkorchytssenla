package com.kkorchyts.Ex6_Knapsack;

// Class for items
public class Item {
    private String name;
    private int weight;
    private int cost;

    // TODO добавить проверки на случай отрицательных веса и стоимости
    public Item(String name, int weight, int cost) {
        setName(name);
        setWeight(weight);
        setCost(cost);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}