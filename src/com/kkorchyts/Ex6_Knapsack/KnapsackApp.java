package com.kkorchyts.Ex6_Knapsack;


import java.util.ArrayList;
import java.util.List;

public class KnapsackApp {
    private List<Item> items;

    public KnapsackApp() {
        items = new ArrayList<Item>();
    }

    public static void main(String[] args) {
        KnapsackApp app = new KnapsackApp();
        app.initalize();
        app.run();
    }

    public void initalize() {
        // TODO: неправильное поведение, если ввести отрицательные вес и стоимость
        items.add(new Item("Отвертка", 1, 2));
        items.add(new Item("Шуруповерт", 5, 50));
        items.add(new Item("IPhone X", 3, 1000));
        items.add(new Item("Книга \"Spring in Action, 4th edition\"", 4, 100));
        items.add(new Item("Шоколадка Спартак", 2, 2));
        items.add(new Item("Кружка", 3, 6));
        items.add(new Item("Фонарик", 2, 10));
    }

    public void run() {
        int maxWeight = 8;
        Knapsack knapsack = new Knapsack(maxWeight);
        Packer packer = new Packer();
        knapsack = packer.packItems(items, knapsack);
        printItemList(items, "Базовый набор вещей");
        printItemList(knapsack.getItems(), "Набор вещей с сумарным весом не более " + maxWeight);
    }

    // TODO: Добавить в вывод текущий общий вес и массу
    // TODO: ??? Возможно, вынести жданный метод в class Knapsack ???
    public void printItemList(List<Item> items, String listName) {
        System.out.println("-------- Start Print \"" + listName + "\" --------");
        if (items != null) {
            for (Item item : items) {
                System.out.println("Название: " + item.getName() + "; Вес: " + item.getWeight() + "; Стоимость: " + item.getCost());
            }
        }
        System.out.println("-------- End Print \"" + listName + "\"  --------");
    }
}
