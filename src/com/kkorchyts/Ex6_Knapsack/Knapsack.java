package com.kkorchyts.Ex6_Knapsack;

import java.util.ArrayList;
import java.util.List;

public class Knapsack {
    private int maxWeight;
    private List<Item> items;

    public Knapsack(int maxWeight) {
        this.maxWeight = maxWeight;
        items = new ArrayList<Item>();
    }

    public boolean replaceItems(List<Item> ItemList) {
        if (ItemList == null)
            return false;

        if (Knapsack.isSuitableItemList(ItemList, getMaxWeight())) {
            items.clear();
            items.addAll(ItemList);
            return true;
        } else
            return false;
    }

    public static boolean isSuitableItemList(List<Item> ItemList, int max_weight) {
        if (ItemList == null)
            return true;

        int weight = 0;

        for (Item item : ItemList) {
            if (item != null)
                weight += item.getWeight();
        }

        return weight <= max_weight;
    }

    public static int getItemListCost(List<Item> ItemList) {
        if (ItemList == null)
            return 0;

        int cost = 0;

        for (Item item : ItemList) {
            if (item != null)
                cost += item.getCost();
        }
        return cost;
    }

    public static int getItemListWeight(List<Item> ItemList) {
        if (ItemList == null)
            return 0;

        int weight = 0;

        for (Item item : ItemList) {
            if (item != null)
                weight += item.getWeight();
        }
        return weight;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public List<Item> getItems() {
        return items;
    }

    public int getCurrentCost() {
        return getItemListCost(items);
    }

    public int getCurrentWeight() {
        return getItemListWeight(items);
    }

}
